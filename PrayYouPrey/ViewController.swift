//
//  ViewController.swift
//  PrayYouPrey
//
//  Created by Igor Medelian on 8/16/18.
//  Copyright © 2018 Igor Medelian. All rights reserved.
//

import UIKit

enum Power {
    case stone
    case scissors
    case paper
    
    func isMorePowerfulThan(_ anotherPower: Power) -> Bool {
        if self == .scissors && anotherPower == .paper {
            return true
        }
        if self == .stone && anotherPower == .scissors {
            return true
        }
        if self == .paper && anotherPower == .stone {
            return true
        }
        return false
    }
}

class Preyer {
    let name: String
    var power: Power?
    var wins: Int
    
    init(name: String, wins: Int = 0) {
        self.name = name
        self.wins = wins
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var preyLabel: UILabel!
    
    var preyers: [Preyer] = [Preyer(name: "Svala"),
                             Preyer(name: "Sasha A"),
                             Preyer(name: "Sasha B"),
                             Preyer(name: "Nikita"),
                             Preyer(name: "Lena"),
                             Preyer(name: "Andry"),
                             Preyer(name: "Igor"),
                             Preyer(name: "Eugene"),
                             Preyer(name: "Sergey"),
                             Preyer(name: "Yura")
                            ]
    
    let powersArray: [Power] = [.stone, .scissors, .paper]
    
    @IBAction func fightButtonPressed(_ sender: UIButton) {
        preyers.forEach { preyer in
            for anotherPreyer in preyers where preyer.name != anotherPreyer.name {
                
                print("\(preyer.name) is fighting against \(anotherPreyer.name)")
                
                // preyer.power = powersArray.randomElement() // for swift >= 4.2
                let randInd = Int(arc4random_uniform(UInt32(powersArray.count)))
                preyer.power = powersArray[randInd]
                
                let randIndex = Int(arc4random_uniform(UInt32(powersArray.count)))
                anotherPreyer.power = powersArray[randIndex]
                
                if preyer.power!.isMorePowerfulThan(anotherPreyer.power!) {
                    preyer.wins += 1
                    print("\(preyer.name) won")
                } else {
                    print("\(preyer.name) lost")
                }
            }
            print("=================================== \(preyer.name) won \(preyer.wins) times")
        }
        
        let sortedPreyers = preyers.sorted { $0.wins > $1.wins }
        preyLabel.text = sortedPreyers.first?.name
        preyLabel.isHidden = false
    }
}

